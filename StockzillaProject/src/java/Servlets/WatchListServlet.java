/**
 * ****************************** Documentation ********************************
 * WatchListServlet.java
 * 
 * @author David Wong
 * @since 27MAY2016
 * 
 * TODO:
 * - Add search functionality
 * - Get real username
 * - Fix the decimal place display on price and days change
 * - Test all the functionality THOROUGHLY
 * 
 * Updates:
 * 29MAY2016
 * - Implemented full functionality of pulling from actual database
 * - Commented the crap out of this thing in case I fall into the sky
 * - Cleaned up code
 * 
 * Purpose:
 * - Handles refreshing of watchlist.jsp page by redirecting to watchlistRedirect.jsp 
 *   after populating the user's watchlist with information from the stocks and 
 *   watchlist tables from the stockzilla database.
 * - Handles removal of a stock from a user's watchlist from the watchlist table
 *   in the stockzilla database.
 * 
 * Functionality:
 * - Accessing watchlist page via the navigation bar will bring the user to the 
 *   watchlist.jsp page which on load will call this servlet with doGet() to 
 *   populate a local list of stocks to display. The user will then be redirected 
 *   at the end of the doGet() to the watchlistRedirect.jsp page where the stock
 *   display will be refreshed.
 * - While on the watchlistRedirect.jsp page, when the user tries to remove a stock
 *   from their watchlist, a doPost() will trigger and this servlet will access the
 *   watchlist tablein the stockzilla database and remove the entry. The user will
 *   then be redirected to watchlistRedirect.jsp where the display will be refreshed.
 * 
 * Dependencies:
 * - Stock.java
 * - watchlist.jsp
 * - watchListRedirect.jsp
 * - DButility.java 
 *      -> getWatchList()
 *          -> stocks table, watchlist table, in stockzilla database
 */
// ****************************** Packages ***********************************\\
package Servlets;

// ****************************** Imports ************************************\\
// Imports for handling front end, communication, and exception handling
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
// Imports for list functionality
import java.util.ArrayList;
import java.util.List;
// Imports for objects
import Beans.Stock;
import Utility.DButility;
import javax.servlet.http.HttpSession;

// **************************** Implementation *******************************\\

public class WatchListServlet extends HttpServlet {
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /* Variables
        action    - used to figure out what called this method
        url       - used to hold the url to redirect to
        user      - used to figure out which watchlist to pull from
        stockList - used to return a populated list for the jsps to display
        */
        String action = request.getParameter("action");
        String url = "/watchlistRedirect.jsp";
        String user;
        List<Stock> stockList = new ArrayList<Stock>();
        
        // Figure out what called this method
        if(action.equals("redirect")){
            // Get the currently logged in user
            HttpSession session = request.getSession(false);
            user = (String)session.getAttribute("paramForLater");// param for later is the universal key to get the Username
            // Populate the list
            stockList = DButility.getWatchList(user);
            // Return the list
            request.setAttribute("stockList", stockList);
            // Redirect user
            getServletContext()
                    .getRequestDispatcher(url)
                    .forward(request, response);
        // Default action, redirect user to a "safe" page
        }else{
            url = "/watchlist.jsp";      
        }
        // Redirect user
        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /* Variables
        action    - used to figure out what called this method
        stockName - used to figure out which stock to add/remove
        url       - used to hold the url to redirect to
        user      - used to figure out which watchlist to pull from
        stockList - used to return a populated list for the jsps to display
        */
        String action = request.getParameter("action");
        String stockName = request.getParameter("index");
        String url = "/watchlistRedirect.jsp";
        String user;
        List<Stock> stockList = new ArrayList<Stock>();
        
        // Get username
        HttpSession session = request.getSession(false);
        user = (String)session.getAttribute("paramForLater");// param for later is the universal key to get the Username
        // Figure out what called this method
        // Remove from watchlist
        if(action.equals("remove")){
            stockList = DButility.removeFromWatchlist(user, stockName);
            request.setAttribute("stockList", stockList);
        // Add to watchlist
        }else if(action.equals("add")){
            stockList = DButility.addToWatchlist(user, stockName);
            request.setAttribute("stockList", stockList);
        // Default action, redirect user to a "safe" page    
        }else{
            url = "/watchlist.jsp";
        }
        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}