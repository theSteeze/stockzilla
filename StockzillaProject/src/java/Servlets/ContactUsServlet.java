/*
THINGS TO DO
- update username for contact us message
*/
package Servlets;

import Beans.ContactUsMessage;
import Beans.User;
import Utility.DButility;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Deepak
 */
public class ContactUsServlet extends HttpServlet {


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
    }
        
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
    {
        doPost(request,response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
    {
        //base url
        String url = "/contact.jsp";
        
        //action that gets parameters passed
        String action = request.getParameter("action");
        
        System.out.println("action: " + action);
        boolean status = false;
        ContactUsMessage messageBean;
        
        if(action.equals("contactUsSubmit"))
        {
            HttpSession session = request.getSession(false);
            String username = (String)session.getAttribute("paramForLater");
            
            System.out.println("Contact Us Page");
            String subject = request.getParameter("contactUsSubject");
            String message = request.getParameter("contactUsMessage");
            messageBean = new ContactUsMessage(username,subject,message);
            System.out.println("username = " + username + ". subject = " + subject + ". message = " + message);
            status = DButility.createMessage(messageBean);
            System.out.println("ContactUsUtility.add status: " + status);
            // succesful login
            if(status == true){
                // pull user info from DB for userHomePage construction
                url =  "/contactConfirm.jsp";
            }else{
                message = "There was a problem sending your message.";
                url = "/contact.jsp";
            }
            
        }else if(action.equals("contactConfirmSubmit"))
        {
                url =  "/contact.jsp";
        }
        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);
    }

    public String getServletInfo() 
    {
        return "Short description";
    }// </editor-fold>

}
