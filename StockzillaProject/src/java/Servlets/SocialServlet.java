/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import beans.*;

import static Utility.DButility.*;


/**
 *
 * @author rodney
 */
public class SocialServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = "/social.jsp";
        
        String action = request.getParameter("action");
        
        if(action.equals("search-username")){
            String userToSearch = request.getParameter("search-username-textbox");
            if(/*findUser(userToSearch)*/ true){
                User searchedUsername = getUser(userToSearch);
                PrintWriter out = response.getWriter();
                out.print("<table>\n" +
"                           <tr>\n" +
"                               <td>${searchedUsername.getUsername()}</td>\n" +
"                               <td>${searchedUsername.getFunds()}</td>\n" +
"                            </tr>\n" +
"                           </table>");
            }
            else{
                PrintWriter out = response.getWriter();
                out.print("No user found.");
            }
        }
        
        //String activeUsername = getUsername();
        //List<User> friendList = new ArrayList<User>();
        //friendList = getFriends(activeUsername);
        //request.setAttribute("friendList", friendList);
        
        getServletContext().getRequestDispatcher(url).forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
