package Servlets;

import Beans.User;
import Utility.DButility;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author bents
 */
public class AcctAuthServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request,response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //base url
        String url = "/login.jsp";
        //action that gets parameters passed
        String action = request.getParameter("action");
        
        System.out.println("action: " + action);
        // message to be displayed in case of error
        String message = "";
        //creating display name for session scope
        String displayName = request.getParameter("username");
        request.setAttribute("username", displayName);
        
        if(action == null){
            action = "login";
        }else if(action.equals("login")){
            boolean status = false; //status of db query
            System.out.println("IM HERE");
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            
            //check if login credentials are correct
            try {
                status = DButility.authLogin(username, password);
                System.out.println("DButility.authUser status: " + status);
            } catch (SQLException ex) {
                Logger.getLogger(AcctAuthServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            // succesful login
            if(status == true){
                // pull user info from DB for userHomePage construction
                String paramForLater = username;
                HttpSession session = request.getSession(true);
                session.setAttribute("paramForLater", username);
                try {
                    DButility.addToLoggedIn(paramForLater);
                } catch (SQLException ex) {
                    Logger.getLogger(AcctAuthServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                url =  "/userHome.jsp";
            }else{
                message = "The credentials you entered do not match our recorsd";
                url = "/login.jsp";
            }
        }else if(action.equals("logout")){
            HttpSession session=request.getSession();  
            session.invalidate();
            DButility.deleteFromLoggedIn();
            url = "/login.jsp";
        }else if(action.equals("register")){
            boolean status = false; // for DB queries
            
            String firstName = request.getParameter("firstName");
            String lastName = request.getParameter("lastName");
            String username = request.getParameter("username");
            String email = request.getParameter("email");
            String password = request.getParameter("password");
            String paramForLater = username;
            HttpSession session = request.getSession(true);
            session.setAttribute("paramForLater", username);
            User user = new User(firstName, lastName, email, username, password);
            
            // check to make sure user entered credentials for all boxes
            if(firstName == null || lastName == null || 
                    email == null || username == null || password == null){
                message = "Please fill out all the text boxes.";
                url = "/register.jsp";
            }
            
            // check to see if username or email already belong to another user
            try {
                status = DButility.authRegister(username, email);
            } catch (SQLException ex) {
                Logger.getLogger(AcctAuthServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            System.out.println("Auth User Status: " + status);
            
            if(status == false){    // successful login
                message = "";
                url = "/thanks.jsp";
                try {
                    DButility.createUser(user);
                } catch (SQLException ex) {
                    Logger.getLogger(AcctAuthServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
            }else if(status == true){
                message = "That username or email already exists.";
                url = "/register.jsp";
            }
            System.out.println("Create User(false = created): " + status);
            request.setAttribute("user", user);
            request.setAttribute("message", message);
        }
        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}