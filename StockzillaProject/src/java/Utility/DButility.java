/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utility;

import Beans.ContactUsMessage;
import Beans.Stock;                 // Used for watchlist, Used in getWatchList().
import Beans.User;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;

public class DButility {
    private static Connection getConnection(){
        Connection conn = null;
        try{
            String url = "jdbc:mysql://127.0.0.1:3306/stockzilla";
            String username = "admin";
            String password = "password";
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, password);
        }catch(Exception e){
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
        return conn;
    }
    
    public static boolean authLogin(String username, String password) throws SQLException{
        boolean status = false;
       
        List<User> users = getUsers();
        for(User u : users){
            if(u.getUsername().equals(username) && u.getPassword().equals(password)){
                status = true;
            }
        }
        return status;
    }
    
    public static boolean authRegister(String username, String email) throws SQLException{
        boolean status = false;
       
        List<User> users = getUsers();
        for(User u : users){
            if(u.getUsername().equals(username) || u.getEmail().equals(email)){
                status = true;
            }
        }
        return status;
    }
  
    public static boolean createUser(User newUser) throws SQLException
    {
        boolean status = false;
       
            Connection conn = getConnection();
            try{
                String insertStmt = "INSERT INTO users" + "(firstname, lastname, email, username, password, buyingPower)" 
                        + "VALUES (?,?,?,?,?,10000)";//table name;

                PreparedStatement ps = conn.prepareStatement(insertStmt);
                ps.setString(1, newUser.getFirstName());
                ps.setString(2, newUser.getLastName());
                ps.setString(3, newUser.getEmail());
                ps.setString(4, newUser.getUsername());
                ps.setString(5, newUser.getPassword());
                ps.executeUpdate();
                ps.close();
                conn.close();
                status = true;


            }catch(Exception e){
                e.printStackTrace();
                System.err.println(e.getMessage());
            }

        return status;
    }
    public static boolean deleteUser(String username){
        boolean status = false;
        try{
            Connection conn = getConnection();
            String prepStmt = "DELETE from users WHERE username = ?";
            PreparedStatement ps = conn.prepareStatement(prepStmt);
            System.out.println("this is username form delete: " + username);
            ps.setString(1, username);
            ps.executeUpdate();
            ps.close();
            conn.close();
            status = true;
            
        }catch(Exception e){
            e.printStackTrace();
            System.err.println(e.getMessage());
        }
        
        return status;
    }
    
    public static List<User> getUsers() throws SQLException{
            Connection conn = getConnection();
            User user = null;
            List<User> users = new ArrayList<User>();
            try{
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery("Select * From users");
                while(rs.next()){
                    String firstName = rs.getString("firstName");
                    String lastName = rs.getString("lastName");
                    String username = rs.getString("username");
                    String email = rs.getString("email");
                    String password = rs.getString("password");

                    user = new User(firstName,lastName,email, username, password);
                    users.add(user);
                }
                rs.close();
                stmt.close();
                conn.close();
            }catch(Exception e){

            }finally{
                try{
                    if(conn!=null){
                        conn.close();
                    }
                }catch(Exception e){

                }
            }
            return users;
        }  

    public static boolean updateUser(String username, String email){
            boolean status = false;
            try{
                Connection conn = getConnection();
                String prepStmt = "UPDATE users SET email = ? WHERE username = ?";
                PreparedStatement ps = conn.prepareStatement(prepStmt);
                ps.setString(1, email);
                ps.setString(2, username);
                ps.executeUpdate();
                ps.close();
                conn.close();
                status = true;

            }catch(Exception e){
                e.printStackTrace();
                System.err.println(e.getMessage());
            }

            return status;
        }
    
    public static boolean createMessage(ContactUsMessage newMessage) 
    {
        boolean status = false;
       
            Connection conn = getConnection();
            try{
                String insertStmt = 
                        "INSERT INTO contact" + "(username, subject, message)" 
                        + " VALUES (?,?,?)";//table name;

                PreparedStatement ps = conn.prepareStatement(insertStmt);
                ps.setString(1, newMessage.getUsername());
                ps.setString(2, newMessage.getSubject());
                ps.setString(3, newMessage.getMessage());
                ps.executeUpdate();
                ps.close();
                conn.close();
                status = true;
                }catch(Exception e){
                e.printStackTrace();
                System.err.println(e.getMessage());
                }
        return status;
    }
    
    /**
     * On an add/remove, updates watchlist and returns a list of stocks on the 
     * user's current watchlist. Default action just gets the watchlist without
     * modification.
     * 
     * Assumes the tables in the database have the following:
     * 
     * watchlist table contains the following fields:
     * username(VARCHAR(45)) stockname(VARCHAR(45))
     * 
     * stocks table contains the following fields:
     * stocksymbol(VARCHAR(45)) stockname(VARCHAR(45)) price(DOUBLE) dayhighprice(DOUBLE) daylowprice(DOUBLE)
     * 
     * @param user - the username to search for in the watchlist table of the database
     * @param stockName - the name of the stock to add/remove
     * @param action - legal actions are: get|add|remove, will default to get
     * @return - an up-to-date list of stocks pulled from the watchlist table of the database
     */
    private static List<Stock> updateWatchList(String user, String stockName, String action){
        /* Variables
        stockList       - holds the list of stocks to be returned
        stockLookupList - holds list of stock names to look up on the database
        conn            - connection to database server
        stmt            - used to hold the statement return from database
        queryStmt       - used to hold the query to database
        rslt            - result set returned from query
        ptrStock        - a pointer used to set values of a new stock before adding
                          it to stockList
        */
        List<Stock> stockList = new ArrayList<Stock>();
        List<String> stockLookupList = new ArrayList<String>();
        Connection conn;
        Statement stmt;
        String queryStmt;
        ResultSet rslt;
        Stock ptrStock;
        
        try {
            // Establish connection
            conn = getConnection();
            // Create a statement
            stmt = conn.createStatement();
            // Figure out which action to perform
            if(action.equals("add")){
                queryStmt = "INSERT INTO watchlist(username, stockname) VALUES("
                            + user + "," + stockName + ")";
                stmt.executeUpdate(queryStmt);
            // Execute query to delete stock from watchlist
            }else if(action.equals("remove")){
                queryStmt = "DELETE FROM watchlist WHERE username='"+user+ "' AND stockname='"+stockName+"';";
                stmt.executeUpdate(queryStmt);
            }
            // Execute query to get watchlist
            queryStmt = "SELECT * FROM watchlist WHERE username='" + user + "'";
            rslt = stmt.executeQuery(queryStmt);
            // Search watchlist table in database for username, put the names of the stock into stockLookupList
            while(rslt.next()){
                // Assuming the watchlist table is setup as described above, 2 should be the 'stockname' field
                stockLookupList.add(rslt.getString("stockname"));
            }
            // Iterate through the stockLookupList and get the relevant stocks from the stocks database
            rslt.close();
            for(String stock : stockLookupList){
                // Execute query to get up-to-date watchlist
                queryStmt = "SELECT * FROM stocks WHERE stockname='" + stock + "'";
                rslt = stmt.executeQuery(queryStmt);
                while(rslt.next()){
                    // Create stock, will only set what's needed for watchlist functionality
                    ptrStock = new Stock();
                    ptrStock.setSymbol(rslt.getString("stocksymbol"));
                    ptrStock.setName(rslt.getString("stockname"));
                    ptrStock.setPrice(rslt.getDouble("price"));
                    ptrStock.setDayhigh(rslt.getDouble("dayhigh"));
                    ptrStock.setDaylow(rslt.getDouble("daylow"));
                    // Add to list
                    stockList.add(ptrStock);
                }
            }
            // Close access to database
            rslt.close();
            stmt.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(DButility.class.getName()).log(Level.SEVERE, null, ex);
        }
        return stockList;
    }
    
    /**
     * Wrapper method for getting a watchlist
     * 
     * @param user - the username to search for in the watchlist table of the database
     * @return - an up-to-date list of stocks pulled from the watchlist table of the database
     */
    public static List<Stock> getWatchList(String user){
        return updateWatchList(user, "", "get");
    }
    
    /**
     * Wrapper method for adding a stock to watchlist
     * 
     * @param user - the username to search for in the watchlist table of the database
     * @param stockName - the name of the stock to add
     * @return - an up-to-date list of stocks pulled from the watchlist table of the database
     */
    public static List<Stock> addToWatchlist(String user, String stockName){
        return updateWatchList(user, stockName, "add");
    }
    
    /**
     * Wrapper method for removing a stock to watchlist
     * 
     * @param user - the username to search for in the watchlist table of the database
     * @param stockName - the name of the stock to remove
     * @return - an up-to-date list of stocks pulled from the watchlist table of the database
     */
    public static List<Stock> removeFromWatchlist(String user, String stockName){
        return updateWatchList(user, stockName, "remove");
    }
    
    public static boolean addToLoggedIn(String username) throws SQLException
    {
        boolean status = false;
       
            Connection conn = getConnection();
            try{
                String insertStmt = "INSERT INTO logged" + "(id, username)" 
                        + "VALUES (?,?)";//table name;

                PreparedStatement ps = conn.prepareStatement(insertStmt);
                ps.setString(1, "0");
                ps.setString(2, username);
                ps.executeUpdate();
                ps.close();
                conn.close();
                status = true;


            }catch(Exception e){
                e.printStackTrace();
                System.err.println(e.getMessage());
            }

        return status;
    }
    
    public static boolean deleteFromLoggedIn(){
        boolean status = false;
        String id = "0";
        try{
            Connection conn = getConnection();
            String prepStmt = "DELETE from logged WHERE id = ?";
            PreparedStatement ps = conn.prepareStatement(prepStmt);
            ps.setString(1, id);
            ps.executeUpdate();
            ps.close();
            conn.close();
            status = true;
            
        }catch(Exception e){
            e.printStackTrace();
            System.err.println(e.getMessage());
        }
        
        return status;
    }
    
    public static String getUsername(){
        String id = "0";
        Connection conn = getConnection();
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	String query = "SELECT * FROM logged where id = ?";
   	 String username = "";
    	try {
        	ps = conn.prepareStatement(query);
        	ps.setString(1, id);
        	rs = ps.executeQuery();
        	while( rs.next() ){
                    if(id.equals(rs.getString("id"))){
                        username = rs.getString("username");
                    }
        	}
                
                rs.close();
                ps.close();
                conn.close();
        	return username;
                
               
    	} catch (SQLException | NullPointerException e) {
        	System.out.println(e);
        	return null;
    	} 
    }
    
    public static User getUser(String usernameToGet){
        User userToReturn = null;
        Connection conn = getConnection();
        
        try{
            PreparedStatement ps = conn.prepareStatement("SELECT from users WHERE username=?");
            ps.setString(0, usernameToGet);
            ResultSet rs = ps.executeQuery();
            String firstName = rs.getString("firstName");
            String lastName = rs.getString("lastName");
            String username = rs.getString("username");
            String email = rs.getString("email");
            String password = rs.getString("password");

            userToReturn = new User(firstName,lastName,email, username, password);
            
        }catch(Exception e){

        }
        
        return userToReturn;
    }
}