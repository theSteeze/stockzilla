/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.io.Serializable;

/**
 * The code in this bean (object) is based on code write by Nathan Hefner and is
 * used under the MIT License.
 *
 * Nathan Hefner (2013) yahoostocks-java [Computer software] Retrieved from 
 * https://github.com/natehefner/yahoostocks-java
 *
 * @author steve eckardt
 */
public class Stock implements Serializable {
    private String name;
    private double shares;
    private String symbol;
    
    private double price = 0.0;
    private int volume = 0;
    private double pe = 0.0;
    private double eps = 0.0;
    private double week52low = 0.0;
    private double week52high = 0.0;
    private double daylow = 0.0;
    private double dayhigh = 0.0;
    private double movingav50day = 0.0;
    private double marketcap = 0.0;
    private String currency = "";
    private double shortRatio = 0.0;
    private double open = 0.0;
    private double previousClose = 0.0;
    private String exchange = "";
    
    /**
     * Default constructor does nothing but create the stock object
     * 
     * Needed for watchlist, trying to access database for actual stock values
     * instead of accessing stockFetcher() directly to keep encapsulation.
     * - David Wong
     */
    public Stock(){
        
    }
    
    public Stock(String sym){
        this.symbol = sym.toUpperCase();
        this.shares = 0.0;
        stockFetcher();
    }
    
    public Stock(String sym, double shar){
        this.symbol = sym.toUpperCase();
        this.shares = shar;
        stockFetcher();
    }   

    
    
    
    
    public void stockFetcher(){
        try { 

            // Retrieve CSV File
            URL yahoo = new URL("http://finance.yahoo.com/d/quotes.csv?s="+ getSymbol() + "&f=l1vr2ejkghm3j3nc4s7pox");
            URLConnection connection = yahoo.openConnection(); 
            InputStreamReader is = new InputStreamReader(connection.getInputStream());
            BufferedReader br = new BufferedReader(is);  

            // Parse CSV Into Array
            String line = br.readLine(); 
            //Only split on commas that aren't in quotes
            String[] stockinfo = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");

            // Handle Our Data

            this.setPrice(handleDouble(stockinfo[0]));
            this.setVolume(handleInt(stockinfo[1]));
            this.setPe(handleDouble(stockinfo[2]));
            this.setEps(handleDouble(stockinfo[3]));
            this.setWeek52low(handleDouble(stockinfo[4]));
            this.setWeek52high(handleDouble(stockinfo[5]));
            this.setDaylow(handleDouble(stockinfo[6]));
            this.setDayhigh(handleDouble(stockinfo[7]));   
            this.setMovingav50day(handleDouble(stockinfo[8]));
            this.setMarketcap(handleDouble(stockinfo[9]));
            this.setName(stockinfo[10].replace("\"", ""));
            this.setCurrency(stockinfo[11].replace("\"", ""));
            this.setShortRatio(handleDouble(stockinfo[12]));
            this.setPreviousClose(handleDouble(stockinfo[13]));
            this.setOpen(handleDouble(stockinfo[14]));
            this.setExchange(stockinfo[15].replace("\"", ""));

        } catch (IOException e) {
            Logger log = Logger.getLogger(getName()); 
            log.log(Level.SEVERE, e.toString(), e);

        }        
    }//end stockFetcher method  
    
	private double handleDouble(String x) {
		Double y;
		if (Pattern.matches("N/A", x)) {  
			y = 0.00;   
		} else { 
			y = Double.parseDouble(x);  
		}  
		return y;
	}
	
	private int handleInt(String x) {
		int y;
		if (Pattern.matches("N/A", x)) {  
			y = 0;   
		} else { 
			y = Integer.parseInt(x);  
		} 
		return y;
	}    
    
    
    
    
    
    public double getShares(){
        return this.shares;
    }

    public String getExchange(){
        return this.exchange;
    }

    public double getPreviousClose(){
        return this.previousClose;
    }

    public double getOpen(){
        return this.open;
    }

    public double getShortRatio(){
        return this.shortRatio;
    }

    public String getCurrency(){
        return this.currency;
    }

    public String getSymbol() { 
        return this.symbol;		
    } 

    public double getPrice() { 		
        return this.price;		
    } 

    public int getVolume() {    
        return this.volume;     
    } 

    public double getPe() {    
        return this.pe;     
    } 

    public double getEps() { 
        return this.eps;     
    } 

    public double getWeek52low() {    
        return this.week52low;    
    } 

    public double getWeek52high() {  
        return this.week52high;    
    } 

    public double getDaylow() {    
        return this.daylow;    
    } 

    public double getDayhigh() {    
        return this.dayhigh;     
    } 

    public double getMovingav50day() {     
        return this.movingav50day;  
    } 

    public double getMarketcap() { 
        return this.marketcap;
    } 

    public String getName(){
        return this.name;
    }

    /* Was missing setters so I autogenerated a bunch of them.
       Needed for interface between this bean -> database -> watchlist
       - David Wong
    */
    
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param shares the shares to set
     */
    public void setShares(double shares) {
        this.shares = shares;
    }

    /**
     * @param symbol the symbol to set
     */
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * @param volume the volume to set
     */
    public void setVolume(int volume) {
        this.volume = volume;
    }

    /**
     * @param pe the pe to set
     */
    public void setPe(double pe) {
        this.pe = pe;
    }

    /**
     * @param eps the eps to set
     */
    public void setEps(double eps) {
        this.eps = eps;
    }

    /**
     * @param week52low the week52low to set
     */
    public void setWeek52low(double week52low) {
        this.week52low = week52low;
    }

    /**
     * @param week52high the week52high to set
     */
    public void setWeek52high(double week52high) {
        this.week52high = week52high;
    }

    /**
     * @param daylow the daylow to set
     */
    public void setDaylow(double daylow) {
        this.daylow = daylow;
    }

    /**
     * @param dayhigh the dayhigh to set
     */
    public void setDayhigh(double dayhigh) {
        this.dayhigh = dayhigh;
    }

    /**
     * @param movingav50day the movingav50day to set
     */
    public void setMovingav50day(double movingav50day) {
        this.movingav50day = movingav50day;
    }

    /**
     * @param marketcap the marketcap to set
     */
    public void setMarketcap(double marketcap) {
        this.marketcap = marketcap;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @param shortRatio the shortRatio to set
     */
    public void setShortRatio(double shortRatio) {
        this.shortRatio = shortRatio;
    }

    /**
     * @param open the open to set
     */
    public void setOpen(double open) {
        this.open = open;
    }

    /**
     * @param previousClose the previousClose to set
     */
    public void setPreviousClose(double previousClose) {
        this.previousClose = previousClose;
    }

    /**
     * @param exchange the exchange to set
     */
    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

}//end Stock object


