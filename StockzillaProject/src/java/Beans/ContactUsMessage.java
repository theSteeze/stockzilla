/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import java.io.Serializable;

/**
 *
 * @author Deepak
 */
public class ContactUsMessage implements Serializable 
{
    private String username;
    private String subject;
    private String message;
    
    public ContactUsMessage()
    {
    username = "";
    subject = "";
    message = "";
    }

    public ContactUsMessage(String username, String subject, String message) 
    {
        this.username = username;
        this.subject = subject;
        this.message = message;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    
    
    
}
