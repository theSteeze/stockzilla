<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/ZjspHTMLformat.css" />
</head>
<body>    
<jsp:include page="ZmenuBar.jsp" />
<script>
 $('#watchlist').addClass('active');
</script>
<jsp:include page="ZmarginLeft.jsp" />
<div id="content">
<div class="container-fluid text-center centerPage">    
  <div class="row content">
    <div class="col-sm-8 text-left "> 
      <h1>Watch List</h1>
      <hr>
       
      <table>
          <tr class="rowTitle">
              <td class="rowPadding">Stock Symbol</td>  
              <td class="rowPadding">Price</td>
              <td class="rowPadding">Days Changed</td>
              <td class="rowPadding">Remove</td>
          </tr>
            <c:forEach var="stock" items="${stockList}">
            <tr>
                <td class="rowPadding"><c:out value="${stock.getSymbol()}" /></td>
                <td class="rowPadding"><c:out value="$${stock.getPrice()}" /></td>
                <td class="rowPadding"><c:out value="$${stock.getDayhigh() - stock.getDaylow()}" /></td>
                <td class="rowPadding"> <form id="formWatchList" action="WatchListServlet" method="post">
                                                        <input id="action" type="hidden" name="action" value="remove">
                                                        <input id="indexArg" type="hidden" name="index" value="${stock.getName()}">
                                                        <input id="submit" type="submit" value="X" class="btn btn-success">
                                        </form>
                <td> 
                
            </tr>
            </c:forEach>
      </table>
    </div>
  </div>
</div>

</div> 
 <jsp:include page="ZmarginRight.jsp" />
 <jsp:include page="Zfooter.jsp" /> 
</body>
</html>
