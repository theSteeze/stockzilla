<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/ZjspHTMLformat.css" />
</head>
<body>    
 <jsp:include page="ZmenuBarEmpty.jsp" />
 <script>
 $('#registerNeed').addClass('active');
</script>
  <jsp:include page="ZmarginLeft.jsp" /> 
<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-8 text-left centerPage"> 
        
        <div id="notLoggedIn">
        <h1 class="text-center"><strong>Register</strong></h1><br>
            <div class="form-group">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                <div class="well">
                    <form action="AcctAuthServlet" method="post">
                    <input type="hidden" name="action" value="register">
                    <label>First Name:</label><br>
                    <input type="text" name="firstName" value="${user.firstName}"><br>
                    <label>Last Name:</label><br>
                    <input type="text" name="lastName"value="${user.lastName}"><br>
                    <label>Username:</label><br>
                    <input type="text" name="username" value="${user.username}"><br>
                    <label>E-Mail:</label><br>
                    <input type="text" name="email"value="${user.email}"><br>
                    <label>Password:</label><br>
                    <input type="password" name="password"value="${user.password}"><br>
                    <label>Confirm Password:</label>
                    <input type="password" name="confPass"><br>
                    
                        <label></label><br>
                        <input type="submit" class="btn btn-success" value="Register" id="submit"><br>
                    </form>
                </div>
                </div>
                <div class="col-md-4"></div>
            </div>
         </div>
    </div>

  </div>
</div>
</div> 
 <jsp:include page="ZmarginRight.jsp" />
<jsp:include page="Zfooter.jsp" />
</body>
</html>


