<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/ZjspHTMLformat.css" />
</head>
<body>    
<jsp:include page="ZmenuBar.jsp" />
<script>
 $('#myprofile').addClass('active');
</script>
 <jsp:include page="ZmarginLeft.jsp" />
<div id="content">  
<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-8 text-left centerPage"> 
      <h1>Welcome Traders</h1>
      <p>Welcome to Stockzilla, your one stop shop for online paper trading! Compete against the market and build your portfolio, or compete against other traders in our tournament system unlike anything else on the market. Create private tournaments between you and your friends to compete for bragging rights or take a shot at the big leagues and compete in our matchmaking tournament system where we pair you up against other traders of equal skill levels. Win tournaments, unlock titles and trophies to display on your profile! Become the ultimate trader with Stockzilla today!</p>
      <hr>
      <h3>Test</h3>
      <p>Lorem ipsum...</p>
    </div>
    
  </div>
</div>
</div> 
 <jsp:include page="ZmarginRight.jsp" />
<jsp:include page="Zfooter.jsp" />

</body>
</html>
