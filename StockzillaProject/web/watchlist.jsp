<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  
  <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/ZjspHTMLformat.css" />
</head>
<body>    

    <body onload="loadWatchList()">
    <jsp:include page="ZmenuBar.jsp" />
<script>
 $('#watchlist').addClass('active');
</script>
<jsp:include page="ZmarginLeft.jsp" />
<div id="content"> 
<div class="container-fluid text-center">    	
    <form id="formWatchList" action="WatchListServlet" method="get">
        <input id="action" type="hidden" name="action" value="redirect">
    </form>    
    </div>
<script>
    function loadWatchList(){
        document.forms["formWatchList"].submit();
    }
</script>
</div> 
 <jsp:include page="ZmarginRight.jsp" />
 <jsp:include page="Zfooter.jsp" /> 
</body>

</html>
