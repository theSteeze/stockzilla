<!DOCTYPE html>
<html lang="en">
<head>
  <title>Stockzilla Social</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/ZjspHTMLformat.css" />
</head>
<body>    
<jsp:include page="ZmenuBar.jsp" />
<script>
 $('#social').addClass('active');
</script>
 <jsp:include page="ZmarginLeft.jsp" />
<div id="content">  
<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-8 text-left centerPage"> 
      <h1>Social</h1><hr>
     
          <form action="SocialServlet" method="post">
              <input type="hidden" name="action" value="search-username">
              <label>Username: </label>
              <input type="text" name="search-username-textbox">
              <input id="submit-search-button" type="submit" value="search">
          </form>
        <table>
            <c:forEach items="${friendList}" var"Friend">
                <tr>
                    <td>${Friend.username}</td>
                    <td>${Friend.funds}</td>
                </tr>
            </c:foreach>
        </table>
    </div>

  </div>
</div>
</div> 
 <jsp:include page="ZmarginRight.jsp" />
<jsp:include page="Zfooter.jsp" />

</body>
</html>

<!-- Rodney test commit. -->