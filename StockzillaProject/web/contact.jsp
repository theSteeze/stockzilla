<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/ZjspHTMLformat.css" />
</head>
<body>    
<jsp:include page="ZmenuBar.jsp" />
<script>
 $('#contact').addClass('active');
</script>
<jsp:include page="ZmarginLeft.jsp" />
<div id="content">
<div class="container-fluid text-center">
<div class="row content">
<div class="col-sm-8 text-left centerPage"> 
      <h1>Contact Us</h1>
     <form action="ContactUsServlet" method="post">
        <input type="hidden" name="action" value="contactUsSubmit">
        <label>Subject:</label><br>
        <input type="text" name="contactUsSubject" required><br>
        <label>Message:</label><br>
        <textarea type="text" name="contactUsMessage" required rows ="10" cols ="50" ></textarea><br>
        <label></label><br>
        <input type="submit" class="btn btn-success" value="Send" id="submit"><br>
    </form>
</div>

</div>
</div>
</div> 
<jsp:include page="ZmarginRight.jsp" />
<jsp:include page="Zfooter.jsp" />

</body>
</html>
