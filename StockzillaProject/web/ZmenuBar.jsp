<%-- 
    Document   : menuBar
    Created on : May 29, 2016, 12:02:09 AM
    Author     : Deepak
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<header>
   <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#"><font style="color: chartreuse">StockZilla</font></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li id="home"><a href="home.jsp">Home</a></li>
        <li id="trade"><a href="trade.jsp">Trade</a></li>
        <li id="watchlist"><a href="watchlist.jsp">Watchlist</a></li>
        <li id="social"><a href="social.jsp">Social</a></li>
        <li id="contact"><a href="contact.jsp">Contact Us</a></li>
      </ul>
     <form action ="AcctAuthServlet" method="post">
      <ul class="nav navbar-nav navbar-right">
        <li id="myprofile"><a href="userHome.jsp"><span class="glyphicon glyphicon-th-large"></span> My Profile</a></li>
        <li id="user"><a href="userSettings.jsp"><span class="glyphicon glyphicon-user"></span>${paramForLater}</a></li>
        <li id="logout"><a href="AcctAuthServlet?action=logout"><span class="glyphicon glyphicon-off"></span> Logout</a></li>
      </ul>
      </form>
    </div>
  </div>
</nav>
</header>

        
