<%-- 
    Document   : trade
    Created on : May 26, 2016, 9:24:06 PM
    Author     : rodney
--%>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Stockzilla</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/ZjspHTMLformat.css" />
 
  
</head>
<body>    
<jsp:include page="ZmenuBar.jsp" />
<script>
 $('#trade').addClass('active');
</script>
<jsp:include page="ZmarginLeft.jsp" />
<div id="content">      
<div class="container-fluid fill text-center ">    
<div class="row content fill ">
    
     
      <div class="col-sm-1"></div>  
        <div class="col-sm-7 text-left centerPage "> 
            
        <h1 class="text-left">Trade</h1>
      <hr>
      <form role="form">
  <div class="form-group">
      <label for="ticker"><font size="4">Ticker:</font></label>
      <input class="form-control" name="ticker" type="text" placeholder="Enter ticker name">
      <hr>
    <label for="chooseAction"><font size="4">Choose Action:</font></label>
        <select class="form-control" id="chooseAction">
            <option>....</option>
            <option>Buy</option>
            <option>Sell</option>
         </select>
    
    <hr>
      <label><font size="2">Action Type</font></label>  
    <div class="radio">
        <label><input type="radio" name="optradio"><strong>Market</strong> Execute order at current market price</label>
    </div>
    <div class="radio">
        <label><input type="radio" name="optradio"><strong>Limit</strong> Execute order at specified price or better</label>
    </div>
    <hr>
    <label><font size="3">Buying Power:</font></label><h4><small> $XXX,XXX.XX</small></h4><br>
    <label for="quantity"><font size="4">Quantity</font></label>
    <input class="form-control" name="quantity" type="text" placeholder="Enter number of shares you want to trade">
    <hr>
    <label for="rationale"><font size="4">Rationale:</font></label><br>
    <textarea type="text" name="rationale" required rows ="6" cols ="60" ></textarea><br>
    <input type="submit" class="btn btn-primary" value="Execute Order" id="submit"><br><br><br>
   </form>   
</div>    
    </div>
  
    
  </div>
    
</div>
   
    </div> 
 <jsp:include page="ZmarginRight.jsp" />
 <jsp:include page="Zfooter.jsp" />
</body>


</html>