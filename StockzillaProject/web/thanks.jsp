<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/ZjspHTMLformat.css" />
</head>
<body>    
<jsp:include page="ZmenuBar.jsp" />
<script>
 $('#registerNeed').addClass('active');
</script>
 <jsp:include page="ZmarginLeft.jsp" />
<div id="content"> 
<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-8 text-left centerPage"> 
      <h1>Thank you for registering</h1>
      <p>Please take a moment and check to make sure all the information you entered is correct.</p>
      <hr>
      <label>First Name: </label>
           <span>${user.firstName}</span><br>
           <label>Last Name:</label>
           <span>${user.lastName}</span><br>
           <label>Email:</label>
           <span>${user.email}</span><br>
           <label>Username:</label>
           <span>${user.username}</span><br>
           <label>Password:</label>
           <span>${user.password}</span><br>

           <p> Click <a href="login.jsp">HERE</a> to go back to login!</p>
    </div>
  </div>
</div>
</div>
 <jsp:include page="ZmarginRight.jsp" />
<jsp:include page="Zfooter.jsp" />
</body>
</html>
